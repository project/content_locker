/**
 * @file
 * Content locker consent js.
 */

(function ($, window, Drupal) {
  Drupal.behaviors.lockerConsent = {
    attach: function attach(context, settings) {
      function LockerConsent(holder) {
        Drupal.Locker.apply(this, arguments);
      }

      if (typeof Drupal.Locker !== 'undefined') {

        LockerConsent.prototype = Object.create(new Drupal.Locker());
        LockerConsent.prototype.constructor = LockerConsent;

        /**
         * Render consent buttons
         * @param {element} holder html element
         */
        LockerConsent.prototype.render = function (holder) {
          if (holder.length) {
            this.holder = holder;
            let type = this.type;
            let lock = $('<span>')
              .addClass(this.iconClass);
            let choice1 = $('<button>')
              .addClass('cl-control')
              .addClass(`cl-${type}`)
              .addClass('locker-btn locker-bnt-yes')
              .attr('value', '1')
              .html(holder.data('content-locker-btn-yes') ? holder.data('content-locker-btn-yes') : Drupal.t('Yes'));

            let choice2 = $('<button>')
              .addClass('cl-control')
              .addClass(`cl-${type}`)
              .addClass('locker-btn locker-bnt-no')
              .attr('value', '0')
              .html(holder.data('content-locker-btn-no') ? holder.data('content-locker-btn-no') : Drupal.t('No'));

            let wrapRadios = $('<div>')
              .addClass('cl-wrapper')
              .addClass(this.actionClass);


            let errors = $('<div>')
              .addClass(this.errorClass)
              .addClass('element-hidden');
            lock.appendTo(holder);
            errors.appendTo(holder);
            let lockerOptionsGeneral = Drupal.getLockerOptions(settings, 'consent', 'general');
            let lockerText = '';
            if (lockerOptionsGeneral
                    && lockerOptionsGeneral.text) {
              lockerText = lockerOptionsGeneral.text.value;
            }

            $('<div>')
              .addClass(this.contentClass)
              .html(lockerText).appendTo(holder);
            choice1.appendTo(wrapRadios);
            choice2.appendTo(wrapRadios);
            wrapRadios.appendTo(holder);
          }
          else {
            throw new Error(
              'holder should not be empty.'
            );
          }
        };

        /**
         * Check if content is locked
         *
         * @param {element} holder html element
         * @return {*} value
         */
        LockerConsent.prototype.isLocked = function (holder) {
          let control = $('input[name="choice-radio"]:checked', holder);
          if (control && control.length) {
            return control.value;
          }
          return 1;
        };

      }

      let lockerPlaceholder = $('.content-locker');
      if (lockerPlaceholder && lockerPlaceholder.length) {
        lockerPlaceholder.once('content-locker-consent').each(function (i, el) {
          let holder = $(el), type = holder.data('content-locker-type'), lockerConsent;
          if (type === 'consent') {
            let baseOptions, lockerOptions;
            if (typeof Drupal.Locker !== 'undefined') {
              baseOptions = Drupal.getLockerOptions(settings, 'consent', 'base');
              lockerOptions = Drupal.getLockerOptions(settings, 'consent');
              lockerConsent = new LockerConsent(holder);

              if (lockerOptions) {
                let lockerSettings = {
                  type: type,
                  options: lockerOptions,
                  errorClass: 'locker-error',
                  holder: holder,
                  contentClass: 'locker-content',
                  iconClass: 'icon-lock',
                  actionClass: 'locker-actions',
                  lockClass: 'locked-content'
                };

                lockerConsent.setOptions(lockerSettings);
                lockerConsent.isAjax = baseOptions.ajax ? baseOptions.ajax : 0;
                lockerConsent.isCookie = baseOptions.cookie ? baseOptions.cookie : 0;
                lockerConsent.cookieLife = baseOptions.cookie_lifetime ? baseOptions.cookie_lifetime : 0;
                let uuid = Drupal.userId();
                lockerConsent.errorClass = lockerSettings.errorClass;
                lockerConsent.setUserId(uuid);
                lockerConsent.subscribeEvents(holder, lockerConsent);
                lockerConsent.render(holder);
              }

              let radios = $('.locker-btn', holder);
              if (radios && radios.length) {
                radios.on('click', function (e) {
                  e.preventDefault();
                  let value = $(e.target).attr('value');
                  switch (value) {
                    case '1':
                      lockerConsent.hideError(e);
                      lockerConsent.fireEvent(lockerConsent.unlockevent, holder,
                        this.value);
                      return false;
                    case '0':
                      lockerConsent.rejectEvent(e);
                      return false;
                  }
                  return false;
                });

              }

              if (lockerConsent
                && lockerConsent.isLocked(holder)) {
                lockerConsent.fireEvent(lockerConsent.lockevent, holder, lockerConsent.type);
              }
              else {
                lockerConsent.fireEvent(lockerConsent.unlockevent, holder, lockerConsent.type);
              }
            }
          }
        });
      }
    }
  };

})(jQuery, window, Drupal);
